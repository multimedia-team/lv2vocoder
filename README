LV2 Vocoder Plugin

Happy robots use Linux and LV2!

See file gpl.txt for information about license of this package.
See file INSTALL for information about how to compile and install this
package.

= Overview =

Perhaps you don't know what a vocoder is, but I'm sure you have heard
one before. Vocoders are often used to add a robotic effect to vocals
in music.

This vocoder plugin has 2 AUDIO inputs and 1 OUTPUT (of course). The first
INPUT is the "Formant" signal which is usually someone's voice. The second
INPUT is the "Carrier" signal which is some sort of continuous synth sound.
The "Carrier" signal is modulated to the "Formant".
There are a number of controls ports. The first one is the "Number of bands"
which is how many frequency bands to use (current maximum is 16). The rest of
the controls set the level of each individual band and should have a value
between 0.0 and 1.0. The lower numbered bands are lower in frequency the
higher numbered bands are higher in frequency.

= Screenshots =

== Ingen setup ==
First one shows how this plugin looks like when loaded in Ingen, a
realtime modular synthesizer and/or effects processor:

lv2vocoder-ingen.png

Two audio inputs and one audio output are created for outside world
(JACK), then they are connected to plugin audio ports.

== JACK setup ==

The second one shows how Ingen is connected to get some real live
robot sounds.

lv2vocoder-patchage.png

First, soundcard is configured to capture from microphone input. Then
system capture audio ports are connected to Ingen formant input audio
port.

MIDI keyboard is attached to soundcard MIDI input. Then system capture
MIDI port is connected to zynjacku LV2 synths host, with Nekobee LV2
plugin loaded in it. Then nekobee output is connected to Ingen carrier
input.

Live audio capture, nekobee output and vocoder output are then
connected to jack_mixer to allow nice monitor ot signal levels.

jack_mixer audio outputs are connected to system playback ports so
result can be monitored using speakers for example.

= History =

This code is based on version 0.3 of LADSPA plugin created by Josh Green.

LADSPA plugin created by Josh Green is basically an adaption of
Achim Settelmeier's Vocoder program to LADSPA.

Achim Settelmeier's <settel-linux@sirlab.de> Vocoder programs and
Josh Green's LADSPA plugin, can be found at:

http://www.sirlab.de/linux/
